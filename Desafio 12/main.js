class Post{
    constructor(title, date, mainContent){
        this.title = title
        this.date = date
        this.mainContent = mainContent
    }
}
class App{
    constructor(){
        this.posts = []
    }
}
let tempList = JSON.parse(localStorage.getItem('LIST_OF_POSTS'))
let posts = ''
if (!tempList) {
    posts = new App()
}
else {
    posts = tempList
}

let titleInput = document.createElement('input')
titleInput.type = 'text'
let dateInput = document.createElement('input')
dateInput.type = 'date'
let postInput = document.createElement('textarea')
let btn = document.createElement('button')
btn.textContent = 'Cargar'
let formBox = document.getElementById('formBox')

formBox.appendChild(titleInput)
formBox.appendChild(dateInput)
formBox.appendChild(postInput)
formBox.appendChild(btn)


btn.addEventListener('click', ()=>{
    let listOfValues = [titleInput.value, dateInput.value, postInput.value]

    if (listOfValues.length == 3){

        let newPost = new Post(titleInput.value, dateInput.value, postInput.value)
        posts.posts.push(newPost)
        localStorage.setItem('LIST_OF_POSTS', JSON.stringify(posts))

        
    } else{
        alert('Debe completar los tres campos')
    }
})

listOfPosts = JSON.parse(localStorage.getItem('LIST_OF_POSTS'))
console.log(listOfPosts.posts)


let publishedPosts = listOfPosts.posts.reverse()
publishedPosts.map((value)=>{
    let container = document.getElementById('postContainer')
    console.log(container)
    let postDiv = document.createElement('div')
    let title = document.createElement('div')
    let date = document.createElement('div')
    let post = document.createElement('div')

    title.innerHTML = `<h2>${value.title} </h2>`
    date.innerHTML = `<h3>${value.date} </h3>`
    post.textContent = value.mainContent
    postDiv.appendChild(title)
    postDiv.appendChild(date)
    postDiv.appendChild(post)
    container.appendChild(postDiv)

})