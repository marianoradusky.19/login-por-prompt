// Crea un array que contiene CEDEARS para analizar y ordenar de acuerdo a tres criterios
const products = [{product: "AAPL", price: 123, marketCap: 200000 }, {product: "TSLA", price: 903, marketCap: 60 },{product: "AMZN", price: 233, marketCap: 270000 }]
// Esta lista, por ejemplo podría ser la lista de fav de cada usuario

for (let i = 0; i< products.length; i++){
    console.log(products[i])
}

alert("Puedes ordenar la lista de CEDEARS por: \n [1] Alfabéticamente \n[2] Precio (mayor a menor) \n [3] Capitalización de mercado (mayor a menor)")
function ortderBy(criteria, list) {
    // Ordena la lista de CEDEAR de acuerdo al  criterio que se indicó 

    if (criteria === 1) {
        //Ordena alfabeticamente 
        //Se podría añadir un criterio más para que sea ascendente o descendente
        let x = list.sort((a, b) => (a.product > b.product ? 1 : -1))
        console.log(x)
    } else if (criteria === 2) {
        //Ordena por precio
        let x = list.sort((a, b) => (a.price > b.price ? 1 : -1))
        console.log(x)
    } else if (criteria === 3) {
        //Ordena por marketcap
        let x = list.sort((a, b) => (a.marketCap > b.marketCap ? 1 : -1))
        console.log(x)
    }
}
// Pregunta por el criterio
let answer = parseInt(prompt("Escriba 1, 2 o 3 según corresponda"))

ortderBy(answer, products)

