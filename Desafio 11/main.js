// Constanres de almacenamieto local
const DASHBOARD_USERS = ''






// Creo una clase maestra que represente la aplicacion
class App {

    constructor() {
        this.users = []
    }


    addUser(user) {
        this.users.push(user);
    }
}

// Creo una clase que tenga usuarios con sus atributos y metidos
class User{
    constructor(name, email, birthday, password, secPassword){
        this.name = name
        this.email = email
        this.birthday = birthday
        this.password = password
    }
}

let dashboard = new App()

// Creo la aplicación
const tempList = JSON.parse(localStorage.getItem(DASHBOARD_USERS))
console.log(tempList)

if (tempList == null || tempList == undefined){

    localStorage.setItem(DASHBOARD_USERS,JSON.stringify(dashboard));
}else{
    dashboard = tempList
}


// Función para crear registros de usuario

function register(dashboard) {
    let name = document.getElementById('name').value
    let birthday = document.getElementById('birthday').value
    let email =document.getElementById('email').value
    let password = document.getElementById('password').value
    let secPassword = document.getElementById('secPassword').value
    console.log(name)
    console.log(birthday)
    console.log(email)
    console.log(password)


    if (name === '' || name === null || name === undefined) {
        alert("Debe introducir todos los campos")
    }
    if (email === '' || email === null || email === undefined || !email.includes('@')) {
        alert("El email es incorrecto.")
    }

    if (password != secPassword){
        alert('Las contraseñas no son iguales.')
        window.location = './'
    } else{
        alert('Su registro fue exitoso.')
        let newUser = new User(name,email,birthday,password)
        console.log(newUser)

        dashboard.addUser(newUser)
        localStorage.setItem(DASHBOARD_USERS,JSON.stringify(dashboard));
        
    }
}
