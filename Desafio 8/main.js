//Importa clase
import {user} from './user.js'
//Genera las variables necesarias
const username = prompt("Defina un usuario");
const password = prompt("Defina una contraseña");
const names = prompt("Su nombre");
const lastname = prompt("Apellido");
const age = parseInt(prompt("Su edad"));
const mainInterest = prompt("Su mercado de interés");

// Simulador de operaciones de compra y venta de acciones 
function calc(login, opType) {
    let ammount = parseFloat(prompt("¿De cuanto es la inversión?"))
    let commission = ammount*0.0075
    let total = 0
    let opSimulation = {opType:"", ammount: ammount, commission: commission, total: total}
    if (login ==true){
        if (opType==true){
            total = ammount + commission
            opSimulation.opType = "Compra"
            opSimulation.total = total
        }
        if (opType==false){
            total = ammount - commission
            opSimulation.opType = "Venta"
            opSimulation.total = total
        }
        alert(`El totalde tu operación es d ${total}, con una comisión de ${commission}`)
    } else {
        alert("Necesitas estar logeado para poder operar.")
    }
    return opSimulation
}


//Crea usuario ejemplo
const user1 = new user(username, password, names, lastname, age, mainInterest)

//Crea el estado de la op y logea al usuario
const loginStatus = user1.login()
const opType = user1.opType()
//Simula op y guarda el resultado en el historial
user1.opHistory.push(calc(loginStatus, opType))

alert(`Ha simulado ${user1.opHistory.length} operaciones ya. `)

for (let i=0; i < user1.opHistory.length; i++) {
    console.log(user1.opHistory[i])
}