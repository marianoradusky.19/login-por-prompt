
// Clase usuer. Esta clase recolecta los datos del usuario para su posterior uso

class user{
    // Contiene todo lo relacionado a las acciones que puede realizar unusuario 
    constructor(username, password, name, lastname, age, mainInterest){
        // Presenta los atributos del objeto user
        this.username = username;
        this.password = password;
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.mainInterest = mainInterest;
    }
    login() {
        // Comprueba que el usuario registrado y el que está operando sean el mismo
        this.asked_username = prompt("Username");
        this.asked_password = prompt("Password");
    
        if ((this.username==this.asked_username) && (this.password==this.asked_password)){
            return true
        } else {
            return false
        }
    }
    opType() {
        // Registra el tipo de operaciones que desea simular el usuario.
        this.op = prompt("¿Compra o venta?")
        if (this.op.toLowerCase()=='compra'){
            return true
        } 
        if (this.op.toLowerCase()=="venta") {
            return false
        }
    }
}

export {user}