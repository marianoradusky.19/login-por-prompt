
// Importo las clases necesarias para construir el dom
import {App} from './App.js'
import { User } from "./User.js";
import { Sections} from './Sections.js';


// Creo la aplicacion
const dashboard = new App()


// Creo un usuario de testeo
const testUser = new User('Mariano Radusky', 'mariano', 'radusky', '1993-07-19')

dashboard.addUser(testUser)



// Creo las 4 primeras secciones

let stocks = new Sections('Mercado de acciones', 'Acciones', './#')
let bonds = new Sections('Mercado de bonos', 'Bonos', './#')
let crypto = new Sections('Mercado de crypto', 'Cryptos', './#')

dashboard.addSection(stocks)
dashboard.addSection(bonds)
dashboard.addSection(crypto)


// Funcion para inicializar y mostrar en dom

function headerConstructor() {
    
    // Carousel  de activos
    const eqCarousel  = document.querySelector('#eqCarousel')
    eqCarousel.className = 'equityCarouselContainer'
    eqCarousel.textContent = 'Esta sección está en construcción'

}

function cardConstructor() {
    // CConecto con el contenedor 

    const cardContainer = document.querySelector('#menuCardContainer')
    // cardContainer.className = 'menuCardContainer'
    console.log(cardContainer)
    // Loop de elementos de users 
    dashboard.sections.map((section)=> {
        
        // Crea el anchor de cada tarjeta 
        const link = document.createElement('a')
        link.href = section.relPath
        
        // Crea un elemento para contonerlo, la tarjeta


        const card = document.createElement('div')
        card.classList = 'menuCard menuCard1' //Se le asignasu estilo
        
        // Creo el contenedor de texto 

        const cardContent = document.createElement('div')
        cardContent.className = 'cardContent'
        console.log(cardContainer)
        // Creo el h2 para que contenga el nombre de la tarjeta
        const cardContentTitle = document.createElement('h2')
        cardContentTitle.textContent = section.cardName
        

        // Se crea los elementos añadiendo al dom
        cardContainer.appendChild(card)
        card.appendChild(link)
        link.appendChild(cardContent)
        cardContent.appendChild(cardContentTitle)
    })
}

function userBarConstructor() {
    
    // Conecto con la barra para rellenarla de info
    const userBar = document.querySelector('#userBar')
    const userName = document.createElement('span')
    userName.className = 'userNameBar'
    userName.textContent = dashboard.users[0].fullName //Lo que me gustaría es que el nombre del usuario salga en bold, ¿cómo lo haría?
    userBar.textContent = `Bienvenido ${userName.textContent}. En breve esta barra se habilitará con el usuario logeado y habilitará un botón para cargar operaciones financieras`
}

// Inizializo los constructores
headerConstructor()
cardConstructor()
userBarConstructor()