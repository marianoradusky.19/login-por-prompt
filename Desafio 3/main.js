alert("Si no pones papas fritas en la mesa no me callaré")

let password = prompt("Quiero que me hagas caso!")


let pw = password.toLowerCase() //Hago que sea en minusculas
console.log(pw)
while (pw!=="papas fritas") {
    // mientras no se escriba papas fritas se repetira indefinidamente
    // Se prodria poner un i que cuente las repeticiones
    pw =  prompt("Dame papas fritas!") //Pregunta
    pw = pw.toLowerCase() //Asegura que pueda cumplir la condición tal y como fue escrita
}

pwArray = pw.split(' ') //Divide la cadena de texto por un elemento clave, el espacio en este casi

for (i of pwArray){
    // itera en el array. La palabra of se la usa con elementos iterables
    alert(`La palabra ${i} tiene ${i.length} letras.`)
}

